package tk.josergc.computersimulator.trinarycomputer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Trit {
	private final static long serialVersionUID = 0L;
	
	private void writeObject(ObjectOutputStream out) throws IOException {
		out.writeChar(value == 0 ? '0' : value == -1 ? '-' : '+');
	}
	
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		do {
			switch(in.readChar()) {
				case '-': value = -1; break;
				case '+': value = 1; break;
				case '0': value = 0; break;
				default: value = 2;
			}
		} while (value == 2);
	}

	private int value;
	
	protected Trit(int initialValue) {
		value = initialValue;
	}
	
	protected Trit() {
	}
	
	public Trit(Trit b) {
		value = b.value;
	}
	
	/**
	 * Adds the given bit and carry to the current value and modifies the carry
	 * @param b the value to add
	 * @param carry the current carry
	 */
	public void adc(Trit b, Trit carry) {
		switch(value) {
			case -1: {
				
			} break;
			case 0: {
				// a = 0; b = 0; c = 0 -> a = 0; c = 0 
				
			} break;
			case 1: {
				
			} break;
		}
	}
	
	public void reset() {
		value = 0;
	} 
	
	public void set() {
		value = 1;
	} 

	public void unset() {
		value = -1;
	} 
}
