package tk.josergc.computersimulator.binarycomputer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class DWord {
	private final static long serialVersionUID = 0L;
	private Word low;
	private Word high;
	
	private void writeObject(ObjectOutputStream out) throws IOException {
		out.writeObject(high);
		out.writeObject(low);
	}
	
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		high = (Word)in.readObject();
		low = (Word)in.readObject();
	}
	
	protected DWord() {
		low = new Word();
		high = new Word();
	}
	
	protected DWord(Word high, Word low) {
		this.high = high;
		this.low = low;
	}
	
	public void add(DWord b, Bit carry, Bit halfCarry) {
		carry.reset();
		adc(b,carry,halfCarry);
	}
	
	public void adc(DWord b, Bit carry, Bit halfCarry) {
		low.adc(b.low,carry,halfCarry);
		high.adc(b.high,carry,halfCarry);
	}
	
}
