package tk.josergc.computersimulator.binarycomputer;

import java.util.Enumeration;
import java.util.Random;
import java.util.Vector;

public class Tests {
	static Bit carry = new Bit();
	static Bit halfCarry = new Bit();
	static String flags() {
		return new StringBuffer()
			.append(carry.intValue() == 1 ? 'c' : '_')
			.append(carry.intValue() == 1 ? 'h' : '_')
			.toString();
	}
	public static void main(String[] sa) {
		Random r = new Random(System.currentTimeMillis());
		long initial, accumulated = 0;
		Vector<Long> accumulateds = new Vector<Long>(); 
		Vector<Integer> tests = new Vector<Integer>(); 
		
		System.out.println("Byte test starts...");
		carry.reset();
		double opsPerSecond;
		int i1, i2, i3;
		Byte b1 = new Byte(0), b2, b3;
		int nTests = 0x100 + (Math.abs(r.nextInt()) & 0xffff);
		tests.addElement(nTests);
		for (int i = 0; i < nTests; i++) {
			if ((i & 0xff) != b1.intValue()) {
				System.out.println("BYTE INC ERROR: " + Integer.toHexString(i & 0xff) + " => " + Integer.toHexString(b1.intValue()) + ": "+ b1 + " [" + flags() + "]");
				return;
			}
			initial = System.currentTimeMillis();
			b1.inc(carry,halfCarry);
			accumulated += System.currentTimeMillis() - initial;
		}
		opsPerSecond = (((double)nTests) / accumulated);
		accumulateds.addElement(accumulated);
		System.out.println(accumulated + " ms for " + nTests + " incs: " + opsPerSecond + " incs per second");
		
		
		carry.reset();
		accumulated = 0;
		tests.addElement(nTests);
		for (int i = 0; i < nTests; i++) {
			b1 = new Byte(i1 = r.nextInt());
			b2 = new Byte(i2 = r.nextInt());
			i3 = (i1 + i2) & 0xff;
			initial = System.currentTimeMillis();
			b3 = b1.add(b2, carry, halfCarry);
			accumulated += System.currentTimeMillis() - initial;
			if (i3 != b3.intValue()) {
				System.out.println(
					"BYTE ADD ERROR: " + Integer.toHexString(i3) + 
					" => " + Integer.toHexString(b3.intValue()) + ": "+ b3 + 
					" [" + flags() + "]"
					);
				return;
			}
		}
		opsPerSecond = (((double)nTests) / accumulated);
		accumulateds.addElement(accumulated);
		System.out.println(accumulated + " ms for " + nTests + " adds: " + opsPerSecond + " adds per second");
		
		
		System.out.println("Word test starts...");
		carry.reset();
		Word w1 = new Word(0), w2, w3;
		nTests = 0x10000 + (Math.abs(r.nextInt()) & 0xfffff);
		tests.addElement(nTests);
		for (int i = 0; i < nTests ; i++) {
			if ((i & 0xffff) != (w1.intValue() & 0xffff)) {
				System.out.println(
					"WORD ERROR: " + Integer.toHexString(i & 0xffff) + 
						" => " + Integer.toHexString(w1.intValue()) + ": " + w1 + 
						" [" + flags() + "]"
						);
				return;
			}
			initial = System.currentTimeMillis();
			w1.inc(carry,halfCarry);
			accumulated += System.currentTimeMillis() - initial;
		}
		opsPerSecond = (((double)nTests) / accumulated);
		accumulateds.addElement(accumulated);
		System.out.println(accumulated + " ms for " + nTests + " incs: " + opsPerSecond + " incs per second");

		carry.reset();
		accumulated = 0;
		tests.addElement(nTests);
		for (int i = 0; i < nTests; i++) {
			w1 = new Word(i1 = r.nextInt());
			w2 = new Word(i2 = r.nextInt());
			i3 = (i1 + i2) & 0xffff;
			initial = System.currentTimeMillis();
			w3 = w1.add(w2, carry, halfCarry);
			accumulated += System.currentTimeMillis() - initial;
			if (i3 != w3.intValue()) {
				System.out.println(
					"WORD ADD ERROR: " + Integer.toHexString(i3) + 
					" => " + Integer.toHexString(w3.intValue()) + ": "+ w3 + 
					" [" + flags() + "]"
					);
				return;
			}
		}
		opsPerSecond = (((double)nTests) / accumulated);
		accumulateds.addElement(accumulated);
		System.out.println(accumulated + " ms for " + nTests + " adds: " + opsPerSecond + " adds per second");
		
		
		System.out.println("All tests were done!");
		accumulated = 0;
		nTests = 0;
		System.out.println("#Tests " + tests);
		System.out.println("#Acc " + accumulateds);
		Enumeration<Long> el = accumulateds.elements();
		Enumeration<Integer> ei = tests.elements();
		for (i1 = 0; el.hasMoreElements(); i1++) {
			accumulated += el.nextElement();
			nTests += ei.nextElement();
		}
		opsPerSecond = ((double)nTests) / accumulated;
		System.out.println(accumulated + " ms for " + nTests + " tests: " + opsPerSecond + " ops per second");
	} 
}
