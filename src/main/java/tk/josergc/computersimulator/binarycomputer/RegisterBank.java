package tk.josergc.computersimulator.binarycomputer;

public class RegisterBank {
	public Bit carry = new Bit();
	public Bit halfCarry = new Bit();
	public Bit zero = new Bit();
	public Bit negative = new Bit();
	public Bit overflow = new Bit();
	public Bit parity = new Bit();
	public Bit interrupt = new Bit();
	public Bit enabledInterrupts = new Bit();
	public Byte flags = new Byte(
		interrupt,
		enabledInterrupts,
		parity,
		overflow,
		negative,
		zero,
		halfCarry,
		carry
		);
	
	public Word pc = new Word();
	public Word sp = new Word();
	public Word si = new Word();
	public Word di = new Word();
	
	public final static int NUMBER_OF_REGISTERS = 8;
	public Byte[] b = new Byte[NUMBER_OF_REGISTERS];
	public Word[] w = new Word[NUMBER_OF_REGISTERS >> 1];
	public DWord[] d = new DWord[NUMBER_OF_REGISTERS >> 2];
	public RegisterBank() {
		for (int i = 0; i < NUMBER_OF_REGISTERS; i++) {
			b[i] = new Byte();
			if ((i & 1) != 0) {
				w[i >> 1] = new Word(b[i],b[i - 1]);
				if ((i & 2) != 0)
					d[i >> 2] = new DWord(w[i >> 1],w[(i >> 1) - 1]);
			}
		}
	}
}
