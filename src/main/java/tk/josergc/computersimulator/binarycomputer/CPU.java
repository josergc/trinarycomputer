package tk.josergc.computersimulator.binarycomputer;

public class CPU {
	private int opcodeAsInt;
	private Byte h;
	private Byte l;
	private int state = 0;
	private RegisterBank r;
	private MemoryManager mm;
	private boolean noHalted = true;

	public CPU(RegisterBank r, MemoryManager mm) {
		this.r = r;
		this.mm = mm;
	}
	
	private int hl() {
		return (h.intValue() << 8) | l.intValue();
	}
	
	private Byte loadNextByte() {
		try {
			return mm.get(r.pc.intValue());
		} finally {
			r.pc.inc(new Bit(0), new Bit(0));
		}
	}

	private void loadOpcode() {
		opcodeAsInt = loadNextByte().intValue();
		state++;
	}
	
	private void loadH() {
		h = loadNextByte();
		state++;
	}
	
	private void loadL() {
		l = loadNextByte();
		state++;
	}
	
	private void loadInmByteReg(int index) {
		r.b[index].put(loadNextByte());
		state = 0;
	}
	
	private void loadByteRegFromMemory(int index) {
		r.b[index].put(mm.get(hl()));
		state = 0;
	}
	
	private void storeByteRegToMemory(int index) {
		mm.put(hl(),r.b[index]);
		state = 0;
	}

	/**
	 * Adds the immediate value after the opcode to the 8 bit register 
	 * indicated by the given 'index'.
	 * @param index destination register index  
	 */
	private void addInmByteReg(int index) {
		r.b[index].add(loadNextByte(),r.carry,r.halfCarry);
		state = 0;
	}
	
	/**
	 * Adds the immediate value after the opcode plus the current carry to the 
	 * 8 bit register indicated by the given 'index'.
	 * @param index destination register index  
	 */
	private void adcInmByteReg(int index) {
		r.b[index].adc(loadNextByte(),r.carry,r.halfCarry);
		state = 0;
	}

	/**
	 * Copies a byte from the memory which 16 bits address is given immediately 
	 * after the opcode and stores it in the register of 8 bits pointed by the 
	 * given 'reg8Index'.
	 * @param reg8Index destination register index  
	 */
	private void loadByteReg(int reg8Index) {
		switch(state) {
			case 1: { loadH(); } break;
			case 2: { loadL(); } break;
			case 3: { loadByteRegFromMemory(reg8Index); } break;
			default: throw new IllegalStateException("'ld rb" + reg8Index + ",mm[absIndex]' reached unknown state " + state);
		}
	}
	
	/**
	 * Copies a byte from the memory which address is given by the register of 
	 * 16 bits pointed by 'reg16' and places it in the register of 8 bits 
	 * pointed by 'reg8Index'.
	 * @param reg8Index destination register index
	 * @param reg16Index 
	 */
	private void loadByteRegIndexed(int reg8Index, int reg16Index) {
		switch(state) {
			case 1: { h = r.w[reg16Index].getHigh(); } break;
			case 2: { l = r.w[reg16Index].getLow(); } break;
			case 3: { loadByteRegFromMemory(reg8Index); } break;
			default: throw new IllegalStateException("'ld rb" + reg8Index + ",mm[rw[" + reg16Index + "]]' reached unknown state " + state);
		}
	}
	
	/**
	 * Stores the value in the 8 bits register pointer by the given 'regIndex' 
	 * in the memory which address is given immediately after the opcode.
	 * @param reg8Index source register index 
	 */
	private void storeByteReg(int reg8Index) {
		switch(state) {
			case 1: { loadH(); } break;
			case 2: { loadL(); } break;
			case 3: { storeByteRegToMemory(reg8Index); } break;
			default: throw new IllegalStateException("'st rb" + reg8Index + ",mm[absindex]' reached unknown state " + state);
		}
	}
	
	/**
	 * Stores the value in the 8 bits register pointer by the given 'regIndex' 
	 * in the memory which address is given immediately after the opcode.
	 * @param reg8Index source register index 
	 */
	private void storeByteRegIndexed(int reg8Index, int reg16Index) {
		switch(state) {
			case 1: { h = r.w[reg16Index].getHigh(); } break;
			case 2: { l = r.w[reg16Index].getLow(); } break;
			case 3: { storeByteRegToMemory(reg8Index); } break;
			default: throw new IllegalStateException("'st rb" + reg8Index + ",mm[rw[" + reg16Index + "]]' reached unknown state " + state);
		}
	}
	

	// ########################################################################
	// ########################################################################
	// ########################################################################
	// ########################################################################

	private void addInmByteReg0() {
		addInmByteReg(0);
	}
	
	private void addInmByteReg1() {
		addInmByteReg(1);
	}
	
	private void addInmByteReg2() {
		addInmByteReg(2);
	}
	
	private void addInmByteReg3() {
		addInmByteReg(3);
	}
	
	private void addInmByteReg4() {
		addInmByteReg(4);
	}
	
	private void addInmByteReg5() {
		addInmByteReg(5);
	}
	
	private void addInmByteReg6() {
		addInmByteReg(6);
	}
	
	private void addInmByteReg7() {
		addInmByteReg(7);
	}
		
	private void adcInmByteReg0() {
		adcInmByteReg(0);
	}
	
	private void adcInmByteReg1() {
		adcInmByteReg(1);
	}
	
	private void adcInmByteReg2() {
		adcInmByteReg(2);
	}
	
	private void adcInmByteReg3() {
		adcInmByteReg(3);
	}
	
	private void adcInmByteReg4() {
		adcInmByteReg(4);
	}
	
	private void adcInmByteReg5() {
		adcInmByteReg(5);
	}
	
	private void adcInmByteReg6() {
		adcInmByteReg(6);
	}
	
	private void adcInmByteReg7() {
		adcInmByteReg(7);
	}		
	
	private void loadInmByteReg0() {
		loadInmByteReg(0);
	}
	
	private void loadInmByteReg1() {
		loadInmByteReg(1);
	}
	
	private void loadInmByteReg2() {
		loadInmByteReg(2);
	}
	
	private void loadInmByteReg3() {
		loadInmByteReg(3);
	}
	
	private void loadInmByteReg4() {
		loadInmByteReg(4);
	}
	
	private void loadInmByteReg5() {
		loadInmByteReg(5);
	}
	
	private void loadInmByteReg6() {
		loadInmByteReg(6);
	}
	
	private void loadInmByteReg7() {
		loadInmByteReg(7);
	}
		
	private void loadByteReg0() {
		loadByteReg(0);
	}
	
	private void loadByteReg1() {
		loadByteReg(1);
	}
	
	private void loadByteReg2() {
		loadByteReg(2);
	}
	
	private void loadByteReg3() {
		loadByteReg(3);
	}
	
	private void loadByteReg4() {
		loadByteReg(4);
	}
	
	private void loadByteReg5() {
		loadByteReg(5);
	}
	
	private void loadByteReg6() {
		loadByteReg(6);
	}
	
	private void loadByteReg7() {
		loadByteReg(7);
	}
	
	private void loadByteReg0Indexed0() {
		loadByteRegIndexed(0,0);
	}
	
	private void loadByteReg1Indexed0() {
		loadByteRegIndexed(1,0);
	}
	
	private void loadByteReg2Indexed0() {
		loadByteRegIndexed(2,0);
	}
	
	private void loadByteReg3Indexed0() {
		loadByteRegIndexed(3,0);
	}
	
	private void loadByteReg4Indexed0() {
		loadByteRegIndexed(4,0);
	}
	
	private void loadByteReg5Indexed0() {
		loadByteRegIndexed(5,0);
	}
	
	private void loadByteReg6Indexed0() {
		loadByteRegIndexed(6,0);
	}
	
	private void loadByteReg7Indexed0() {
		loadByteRegIndexed(7,0);
	}
	
	private void loadByteReg0Indexed1() {
		loadByteRegIndexed(0,1);
	}
	
	private void loadByteReg1Indexed1() {
		loadByteRegIndexed(1,1);
	}
	
	private void loadByteReg2Indexed1() {
		loadByteRegIndexed(2,1);
	}
	
	private void loadByteReg3Indexed1() {
		loadByteRegIndexed(3,1);
	}
	
	private void loadByteReg4Indexed1() {
		loadByteRegIndexed(4,1);
	}
	
	private void loadByteReg5Indexed1() {
		loadByteRegIndexed(5,1);
	}
	
	private void loadByteReg6Indexed1() {
		loadByteRegIndexed(6,1);
	}
	
	private void loadByteReg7Indexed1() {
		loadByteRegIndexed(7,1);
	}

	private void loadByteReg0Indexed2() {
		loadByteRegIndexed(0,2);
	}
	
	private void loadByteReg1Indexed2() {
		loadByteRegIndexed(1,2);
	}
	
	private void loadByteReg2Indexed2() {
		loadByteRegIndexed(2,2);
	}
	
	private void loadByteReg3Indexed2() {
		loadByteRegIndexed(3,2);
	}
	
	private void loadByteReg4Indexed2() {
		loadByteRegIndexed(4,2);
	}
	
	private void loadByteReg5Indexed2() {
		loadByteRegIndexed(5,2);
	}
	
	private void loadByteReg6Indexed2() {
		loadByteRegIndexed(6,2);
	}
	
	private void loadByteReg7Indexed2() {
		loadByteRegIndexed(7,2);
	}	
	
	private void loadByteReg0Indexed3() {
		loadByteRegIndexed(0,3);
	}
	
	private void loadByteReg1Indexed3() {
		loadByteRegIndexed(1,3);
	}
	
	private void loadByteReg2Indexed3() {
		loadByteRegIndexed(2,3);
	}
	
	private void loadByteReg3Indexed3() {
		loadByteRegIndexed(3,3);
	}
	
	private void loadByteReg4Indexed3() {
		loadByteRegIndexed(4,3);
	}
	
	private void loadByteReg5Indexed3() {
		loadByteRegIndexed(5,3);
	}
	
	private void loadByteReg6Indexed3() {
		loadByteRegIndexed(6,3);
	}
	
	private void loadByteReg7Indexed3() {
		loadByteRegIndexed(7,3);
	}
	
	private void storeByteReg0() {
		storeByteReg(0);
	}
	
	private void storeByteReg1() {
		storeByteReg(1);
	}
	
	private void storeByteReg2() {
		storeByteReg(2);
	}
	
	private void storeByteReg3() {
		storeByteReg(3);
	}
	
	private void storeByteReg4() {
		storeByteReg(4);
	}
	
	private void storeByteReg5() {
		storeByteReg(5);
	}
	
	private void storeByteReg6() {
		storeByteReg(6);
	}
	
	private void storeByteReg7() {
		storeByteReg(7);
	}
	
	private void storeByteReg0Indexed0() {
		storeByteRegIndexed(0,0);
	}
	
	private void storeByteReg1Indexed0() {
		storeByteRegIndexed(1,0);
	}
	
	private void storeByteReg2Indexed0() {
		storeByteRegIndexed(2,0);
	}
	
	private void storeByteReg3Indexed0() {
		storeByteRegIndexed(3,0);
	}
	
	private void storeByteReg4Indexed0() {
		storeByteRegIndexed(4,0);
	}
	
	private void storeByteReg5Indexed0() {
		storeByteRegIndexed(5,0);
	}
	
	private void storeByteReg6Indexed0() {
		storeByteRegIndexed(6,0);
	}
	
	private void storeByteReg7Indexed0() {
		storeByteRegIndexed(7,0);
	}
	
	private void storeByteReg0Indexed1() {
		storeByteRegIndexed(0,1);
	}
	
	private void storeByteReg1Indexed1() {
		storeByteRegIndexed(1,1);
	}
	
	private void storeByteReg2Indexed1() {
		storeByteRegIndexed(2,1);
	}
	
	private void storeByteReg3Indexed1() {
		storeByteRegIndexed(3,1);
	}
	
	private void storeByteReg4Indexed1() {
		storeByteRegIndexed(4,1);
	}
	
	private void storeByteReg5Indexed1() {
		storeByteRegIndexed(5,1);
	}
	
	private void storeByteReg6Indexed1() {
		storeByteRegIndexed(6,1);
	}
	
	private void storeByteReg7Indexed1() {
		storeByteRegIndexed(7,1);
	}

	private void storeByteReg0Indexed2() {
		storeByteRegIndexed(0,2);
	}
	
	private void storeByteReg1Indexed2() {
		storeByteRegIndexed(1,2);
	}
	
	private void storeByteReg2Indexed2() {
		storeByteRegIndexed(2,2);
	}
	
	private void storeByteReg3Indexed2() {
		storeByteRegIndexed(3,2);
	}
	
	private void storeByteReg4Indexed2() {
		storeByteRegIndexed(4,2);
	}
	
	private void storeByteReg5Indexed2() {
		storeByteRegIndexed(5,2);
	}
	
	private void storeByteReg6Indexed2() {
		storeByteRegIndexed(6,2);
	}
	
	private void storeByteReg7Indexed2() {
		storeByteRegIndexed(7,2);
	}	
	
	private void storeByteReg0Indexed3() {
		storeByteRegIndexed(0,3);
	}
	
	private void storeByteReg1Indexed3() {
		storeByteRegIndexed(1,3);
	}
	
	private void storeByteReg2Indexed3() {
		storeByteRegIndexed(2,3);
	}
	
	private void storeByteReg3Indexed3() {
		storeByteRegIndexed(3,3);
	}
	
	private void storeByteReg4Indexed3() {
		storeByteRegIndexed(4,3);
	}
	
	private void storeByteReg5Indexed3() {
		storeByteRegIndexed(5,3);
	}
	
	private void storeByteReg6Indexed3() {
		storeByteRegIndexed(6,3);
	}
	
	private void storeByteReg7Indexed3() {
		storeByteRegIndexed(7,3);
	}
	
	/**
	 * Executes the next step of the current selected opcode. 
	 */
	private void executeOpcode() {
		switch(opcodeAsInt) {
			case 0x00: { // ld r0,mm[absIndex]
				loadByteReg0();
			} break;
			case 0x01: { // ld r1,mm[absIndex]
				loadByteReg1();
			} break;
			case 0x02: { // ld r2,mm[absIndex]
				loadByteReg2();
			} break;
			case 0x03: { // ld r3,mm[absIndex]
				loadByteReg3();
			} break;
			case 0x04: { // ld r4,mm[absIndex]
				loadByteReg4();
			} break;
			case 0x05: { // ld r5,mm[absIndex]
				loadByteReg5();
			} break;
			case 0x06: { // ld r6,mm[absIndex]
				loadByteReg6();
			} break;
			case 0x07: { // ld r7,mm[absIndex]
				loadByteReg7();
			} break;
			
			case 0x08: { // st r0,mm[absIndex]
				loadByteReg0();
			} break;
			case 0x09: { // st r1,mm[absIndex]
				loadByteReg1();
			} break;
			case 0x0a: { // st r2,mm[absIndex]
				loadByteReg2();
			} break;
			case 0x0b: { // st r3,mm[absIndex]
				loadByteReg3();
			} break;
			case 0x0c: { // st r4,mm[absIndex]
				loadByteReg4();
			} break;
			case 0x0d: { // st r5,mm[absIndex]
				loadByteReg5();
			} break;
			case 0x0e: { // st r6,mm[absIndex]
				loadByteReg6();
			} break;
			case 0x0f: { // st r7,mm[absIndex]
				storeByteReg7();
			} break;
			
			default: throw new IllegalStateException("Unknown opcode 0x" + Integer.toHexString(opcodeAsInt));
		}
	}

	// ########################################################################
	// ########################################################################
	// ########################################################################
	// ########################################################################
		
	/**
	 * Executes the next step in the CPU if it is not halted. The execution of 
	 * the next step doesn't mean it is going to execute the next opcode but the 
	 * next phase in the execution of the current opcode.
	 * If there is not selected opcode, it loads the next opcode.
	 */
	public void next() {
		if (noHalted)
			switch(state) {
				case 0: { // Get opcode
					loadOpcode();
				} break;
				default:
					executeOpcode();
			}
	}
}
