package tk.josergc.computersimulator.binarycomputer;

public class MemoryManager {
	private Byte[] memory = new Byte[0x10000];
	
	public MemoryManager() {
		for (int i = 0, fi = memory.length; i < fi; i++)
			memory[i] = new Byte();
	}
	
	public Byte get(int address) {
		return memory[address];
	}
	
	public void put(int address, Byte b) {
		memory[address].put(b);
	}
}
