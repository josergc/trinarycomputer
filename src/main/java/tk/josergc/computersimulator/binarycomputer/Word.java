package tk.josergc.computersimulator.binarycomputer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Word {
	private final static long serialVersionUID = 0L;
	private Byte low;
	private Byte high;
	
	private void writeObject(ObjectOutputStream out) throws IOException {
		out.writeObject(toString());
	}
	
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		high = (Byte)in.readObject();
		low = (Byte)in.readObject();
	}
	
	protected Word() {
		low = new Byte();
		high = new Byte();
	}
	
	protected Word(int initialValue) {
		this(new Byte((initialValue >> 8) & 0xff),new Byte(initialValue & 0xff));
	}
	
	protected Word(Byte high, Byte low) {
		this.high = high;
		this.low = low;
	}
	
	public Word add(Word b, Bit carry, Bit halfCarry) {
		return adc(b,carry.reset(),halfCarry);
	}
	
	public Word adc(Word b, Bit carry, Bit halfCarry) {
		low.adc(b.low,carry,new Bit());
		halfCarry.put(carry);
		high.adc(b.high,carry,new Bit());
		return this;
	}
	
	public Word inc(Bit carry, Bit halfCarry) {
		carry.reset();
		low.inc(carry,halfCarry);
		if (carry.intValue() == 1)
			high.adc(Byte.ZERO,carry,halfCarry);
		return this;
	}

	
	@Override
	public String toString() {
		return new StringBuffer().append(high).append(' ').append(low).toString();
	}

	public int intValue() {
		return (high.intValue() << 8) | low.intValue();
	}

	public Byte getHigh() {
		return high;
	}

	public Byte getLow() {
		return low;
	}
	
}
