package tk.josergc.computersimulator.binarycomputer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Random;

public class Bit implements Serializable {
	private static final long serialVersionUID = 0L;
	
	private void writeObject(ObjectOutputStream out) throws IOException {
		out.writeChar(value + '0');
	}
	
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		do {
			switch(in.readChar()) {
				case '0': value = 0; break;
				case '1': value = 1; break;
				default: value = 2;
			}
		} while (value == 2);
	}
	
	public static final Bit ZERO = new Bit(0);
	public static final Bit ONE = new Bit(1);

	private int value;
	
	protected Bit(int initialValue) {
		value = initialValue;
	}
	
	protected Bit() {
	}
	
	public Bit(Bit b) {
		value = b.value;
	}
	
	/**
	 * Adds the given bit and carry to the current value and modifies the carry
	 * @param b the value to add
	 * @param carry the current carry
	 * @return this bit
	 */
	public Bit adc(Bit b, Bit carry) {
		if (value == 0) {
			if (b.value == 0) {
				if (carry.value == 0) {
					// a = 0; b = 0; c = 0 -> a = 0; c = 0 
				} else {
					value = 1;
					carry.value = 0;
				}
			} else {
				if (carry.value == 0) {
					// a = 0; b = 1; c = 0 -> a = 1; c = 0 
					value = 1;
				} else {
					// a = 0; b = 1; c = 1 -> a = 0; c = 1 
				}
			}
		} else {
			if (b.value == 0) {
				if (carry.value == 0) {
					// a = 1; b = 0; c = 0 -> a = 1; c = 0 
				} else {
					// a = 1; b = 0; c = 1 -> a = 0; c = 1
					value = 0;
					carry.value = 1;
				}
			} else {
				if (carry.value == 0) {
					// a = 1; b = 1; c = 0 -> a = 0; c = 1 
					value = 0;
					carry.value = 1;
				} else {
					// a = 1; b = 1; c = 1 -> a = 1; c = 1
				}
			}
		}
		return this;
	}

	/**
	 * Decreases the current bit adding the given carry and modifies it.
	 * @param the current value of the carry
	 * @return this bit 
	 */
	public Bit dec(Bit carry) {
		if (value == 0) {
			if (carry.value == 0) {
				// a = 0; c = 0 -> a = 1; c = 1
				value = 1;
				carry.value = 1;
			} else {
				// a = 0; c = 1 -> a = 0; c = 1
			}
		} else {
			if (carry.value == 0) {
				// a = 1; c = 0 -> a = 0; c = 0
				value = 0;
			} else {
				// a = 1; c = 1 -> a = 0; c = 1
			}
		}
		return this;
	}
	
	/**
	 * Increases the current bit adding the given carry and modifies it.
	 * @param the current value of the carry
	 * @return this bit 
	 */
	public Bit inc(Bit carry) {
		if (value == 0) {
			if (carry.value == 0) {
				// a = 0; c = 0 -> a = 1; c = 0
				value = 1;
			} else {
				// a = 0; c = 1 -> a = 1; c = 0
				value = 1;
				carry.value = 0;
			}
		} else {
			if (carry.value == 0) {
				// a = 1; c = 0 -> a = 0; c = 1
				value = 0;
				carry.value = 1;
			} else {
				// a = 1; c = 1 -> a = 1; c = 1
			}
		}
		return this;
	}
	
	/**
	 * Subtracts the given bit and carry to the current value and modifies the 
	 * carry
	 * @param b the value to subtract
	 * @param carry the current carry
	 * @return this bit
	 */
	public Bit sbc(Bit b, Bit carry) {
		if (value == 0) {
			if (b.value == 0) {
				if (carry.value == 0) {
					// a = 0; b = 0; c = 0 -> a = 0; c = 0 
				} else {
					// a = 0; b = 0; c = 1 -> a = 1; c = 1
					value = 1;
					carry.value = 1;
				}
			} else {
				if (carry.value == 0) {
					// a = 0; b = 1; c = 0 -> a = 1; c = 1 
					value = 1;
					carry.value = 1;
				} else {
					// a = 0; b = 1; c = 1 -> a = 0; c = 1 
				}
			}
		} else {
			if (b.value == 0) {
				if (carry.value == 0) {
					// a = 1; b = 0; c = 0 -> a = 1; c = 0 
				} else {
					// a = 1; b = 0; c = 1 -> a = 0; c = 0
					value = 0;
					carry.value = 0;
				}
			} else {
				if (carry.value == 0) {
					// a = 1; b = 1; c = 0 -> a = 0; c = 0 
					value = 0;
					carry.value = 0;
				} else {
					// a = 1; b = 1; c = 1 -> a = 1; c = 1
				}
			}
		}
		return this;
	}

	public Bit reset() {
		value = 0;
		return this;
	} 
		
	public Bit set() {
		value = 1;
		return this;
	}
	
	public Bit put(Bit b) {
		value = b.value;
		return this;
	}
	
	public int intValue() {
		return value;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof Bit)
			return value == ((Bit)o).value;
		return false;
	}
	
	@Override
	public String toString() {
		return Integer.toString(value);
	}
}
