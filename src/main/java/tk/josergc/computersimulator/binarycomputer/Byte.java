package tk.josergc.computersimulator.binarycomputer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Byte {
	private final static long serialVersionUID = 0L;
	public final static int NUMBER_OF_BITS = 8;
	public final static Byte ZERO = new Byte(0);
	public final static Byte ONE = new Byte(1);
	private Bit[] bits = new Bit[NUMBER_OF_BITS];
	
	private void writeObject(ObjectOutputStream out) throws IOException {
		out.writeObject(this);
	}
	
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		for (int i = NUMBER_OF_BITS - 1; i >= 0; i--) 
			bits[i] = (Bit)in.readObject();
	}
	
	public Byte() {
		for (int i = 0; i < NUMBER_OF_BITS; i++) 
			bits[i] = new Bit();
	}
	
	public Byte(int initialValue) {
		for (int i = 0; i < NUMBER_OF_BITS; i++) 
			bits[i] = new Bit((initialValue >> i) & 1);
	}
	
	public Byte(Byte b) {
		for (int i = 0; i < NUMBER_OF_BITS; i++) 
			bits[i] = new Bit(b.bits[i]);
	}
	
	public Byte(Bit b7, Bit b6, Bit b5, Bit b4, Bit b3, Bit b2, Bit b1, Bit b0) {
		bits[0] = new Bit(b0);
		bits[1] = new Bit(b1);
		bits[2] = new Bit(b2);
		bits[3] = new Bit(b3);
		bits[4] = new Bit(b4);
		bits[5] = new Bit(b5);
		bits[6] = new Bit(b6);
		bits[7] = new Bit(b7);
	}	
	
	public Byte add(Byte b, Bit carry, Bit halfCarry) {
		carry.reset();
		adc(b,carry,halfCarry);
		return this;
	}
	
	public Byte adc(Byte b, Bit carry, Bit halfCarry) {
		int i = 0; 
		int fi = NUMBER_OF_BITS >> 1;
		while (i < fi) {
			bits[i].adc(b.bits[i], carry);
			i++;
		}
		halfCarry.put(carry);
		fi <<= 1;
		while (i < fi) {
			bits[i].adc(b.bits[i], carry);
			i++;
		}
		return this;
	}
	
	public Byte inc(Bit carry, Bit halfCarry) {
		carry.reset();
		return add(ONE,carry,halfCarry);
	}
	
	public Byte sbc(Byte b, Bit carry, Bit halfCarry) {
		int i = 0; 
		int fi = NUMBER_OF_BITS >> 1;
		while (i < fi) {
			bits[i].sbc(b.bits[i], carry);
			i++;
		}
		halfCarry.put(carry);
		fi <<= 1;
		while (i < fi) {
			bits[i].sbc(b.bits[i], carry);
			i++;
		}
		return this;
	}
	
	public Byte sub(Byte b, Bit carry, Bit halfCarry) {
		carry.reset();
		sbc(b,carry,halfCarry);
		return this;
	}
	
	public int intValue() {
		int value = 0;
		for (int i = 0; i < NUMBER_OF_BITS; i++)
			if (bits[i].intValue() == 1)
				value |= 1 << i; 
		return value;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof Byte) {
			Byte p = (Byte)o;
			int i = 0;
			while (i < NUMBER_OF_BITS && bits[i].equals(p.bits[i])) 
				i++;
			return i == NUMBER_OF_BITS;
		}
		return false;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (int i = NUMBER_OF_BITS - 1; i >= 0; i--)
			sb.append(bits[i]);
		return sb.toString();
	}

	public void put(Byte b) {
		for (int i = 0; i < NUMBER_OF_BITS; i++) 
			bits[i].put(b.bits[i]);
	}

	public int getHighNibble() {
		int value = 0;
		for (int i = NUMBER_OF_BITS >> 1; i < NUMBER_OF_BITS; i++)
			if (bits[i].intValue() == 1)
				value |= 1 << i; 
		return value;
	}

	public int getLowNibble() {
		int value = 0;
		for (int i = 0, fi = NUMBER_OF_BITS >> 1; i < fi; i++)
			if (bits[i].intValue() == 1)
				value |= 1 << i; 
		return value;
	}
}
